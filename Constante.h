//
// Created by Lakouzz on 29/04/2018.
//

#ifndef CALCULATRICE_CONSTANTE_H
#define CALCULATRICE_CONSTANTE_H

#include "Expression.h"

class Constante: public Expression
{

public:
    Constante(float c);
    float calculer();
    void afficher();

protected:
    float _constante;
};


#endif //CALCULATRICE_CONSTANTE_H
