//
// Created by Willoux on 01/05/18.
//

#include "Variable.h"


Variable::Variable(std::string x, float valeur)
{
    _variable = x;
    _valeur = valeur;
}

float Variable::calculer(){
    return _valeur;
}

void Variable::afficher()
{
    std::cout << _variable;
}
