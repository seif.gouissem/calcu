//
// Created by Lakouzz on 29/04/2018.
//

#ifndef CALCULATRICE_EXPRESSION_H
#define CALCULATRICE_EXPRESSION_H

#include <iostream>


class Expression {

public:
    Expression();
    virtual void afficher() = 0;
    //virtual void afficher_npi() = 0;
    virtual float calculer() = 0;
protected:
    Expression* _operandeDroit;
    Expression* _operandeGauche;

};


#endif //CALCULATRICE_EXPRESSION_H
