#include <iostream>

#include "Constante.h"
#include "Variable.h"
#include "Division.h"
#include "Soustraction.h"
#include "Multiplication.h"
#include "Addition.h"
#include <typeinfo>
#include <string.h>

int main() {

    Constante c6(6);
    Variable v3("x", 3);
    Multiplication mv1(c6, v3);

    Constante c1(1);
    Soustraction trois(c1,mv1);
    Constante c20(20);
    Soustraction s1(trois,c20);
    Constante c4(4);
    Constante c5(5);
    Constante c2(2);
    Division d1(c5,c2);
    Addition a1(d1,c4);
    Multiplication m1(s1,a1);


    m1.afficher();
    std::cout << std::endl <<m1.calculer() << std::endl;
    std::cout << typeid(c6).name()<< std::endl;

    const char* pch = strstr(typeid(c6).name(), "Constante");

    std::cout <<pch<< std::endl;

    return 0;
}