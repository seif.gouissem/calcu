//
// Created by Lakouzz on 29/04/2018.
//

#ifndef CALCULATRICE_SOUSTRACTION_H
#define CALCULATRICE_SOUSTRACTION_H

#include "Operateur.h"
class Soustraction : public Operateur
{
public:
    Soustraction(Expression&,Expression&);
    float calculer();
    void afficher();
};


#endif //CALCULATRICE_SOUSTRACTION_H
