//
// Created by Lakouzz on 29/04/2018.
//

#ifndef CALCULATRICE_ADDITION_H
#define CALCULATRICE_ADDITION_H

#include "Operateur.h"
#include "Expression.h"

class Addition :public Operateur
{
public:
    Addition(Expression&,Expression&);
    float calculer();
    void afficher();

private:
};


#endif //CALCULATRICE_ADDITION_H
