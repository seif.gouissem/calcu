//
// Created by Lakouzz on 29/04/2018.
//
#include <typeinfo>
#include <string.h>
#include "Multiplication.h"

Multiplication::Multiplication(Expression &e1, Expression &e2)

{
    _operandeGauche=&e1;
    _operandeDroit=&e2;
}

float Multiplication::calculer()
{
    return _operandeGauche->calculer()*_operandeDroit->calculer();
}

void Multiplication::afficher()
{
    _operandeGauche->afficher();
    if ((dynamic_cast<const Variable*>(_operandeGauche)==nullptr)&&(dynamic_cast<const Variable*>(_operandeDroit)==nullptr))
        std::cout << "*";
    _operandeDroit->afficher();
}