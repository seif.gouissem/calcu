//
// Created by Lakouzz on 29/04/2018.
//

#include "Soustraction.h"

Soustraction::Soustraction(Expression &e1, Expression &e2)
{
    _operandeGauche=&e1;
    _operandeDroit=&e2;
}
float Soustraction::calculer()
{
    return _operandeGauche->calculer()-_operandeDroit->calculer();
}

void Soustraction::afficher()
{
    _operandeGauche->afficher();
    std::cout << "-";
    _operandeDroit->afficher();
}