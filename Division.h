//
// Created by Lakouzz on 29/04/2018.
//

#ifndef CALCULATRICE_DIVISION_H
#define CALCULATRICE_DIVISION_H

#include "Operateur.h"
class Division : public Operateur
{
public:
    Division(Expression&,Expression&);
    float calculer();
    void afficher();
};


#endif //CALCULATRICE_DIVISION_H
