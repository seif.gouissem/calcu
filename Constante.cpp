//
// Created by Lakouzz on 29/04/2018.
//

#include "Constante.h"

Constante::Constante(float c)
{
    _constante = c;
}

float Constante::calculer(){
    return _constante;
}

void Constante::afficher()
{
    std::cout << _constante;
}