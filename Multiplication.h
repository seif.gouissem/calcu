//
// Created by Lakouzz on 29/04/2018.
//

#ifndef CALCULATRICE_MULTIPLICATION_H
#define CALCULATRICE_MULTIPLICATION_H

#include "Operateur.h"
#include "Expression.h"
#include "Variable.h"
class Multiplication :public Operateur
{
public:
    Multiplication(Expression&,Expression&);
    float calculer();
    void afficher();
};


#endif //CALCULATRICE_MULTIPLICATION_H
