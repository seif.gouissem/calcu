//
// Created by Willoux on 01/05/18.
//

#ifndef CALCULATRICE_VARIABLE_H
#define CALCULATRICE_VARIABLE_H

#include "Expression.h"

class Variable: public Expression {

public:
    Variable(std::string x, float val);
    float calculer();
    void afficher();

protected:
    std::string _variable;
    float _valeur;

};


#endif //CALCULATRICE_VARIABLE_H