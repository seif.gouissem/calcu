//
// Created by Lakouzz on 29/04/2018.
//

#include "Addition.h"

Addition::Addition(Expression& e1, Expression& e2)
{
    _operandeGauche = &e1;
    _operandeDroit = &e2;
}

float Addition::calculer()
{
    return _operandeGauche->calculer()+_operandeDroit->calculer();
}

void Addition::afficher()
{
    _operandeGauche->afficher();
    std::cout << "+";
    _operandeDroit->afficher();
}
