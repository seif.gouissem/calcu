//
// Created by Lakouzz on 29/04/2018.
//

#include "Division.h"
Division::Division(Expression &e1, Expression &e2)
{
    _operandeGauche=&e1;
    _operandeDroit=&e2;
}
float Division::calculer()
{
    return _operandeGauche->calculer()/_operandeDroit->calculer();
}

void Division::afficher()
{
    _operandeGauche->afficher();
    std::cout << "/";
    _operandeDroit->afficher();
}